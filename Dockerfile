FROM docker:19.03-git
RUN apk update && apk add --no-cache \
    bash
RUN git -c advice.detachedHead=false clone --branch 3.0.0 https://github.com/fsaintjacques/semver-tool.git /tmp/semver-tool && \
    cp /tmp/semver-tool/src/semver /usr/local/bin && \
	chmod a+x /usr/local/bin/semver && \
	rm -rf /tmp/semver-tool && \
	mkdir /work
WORKDIR /work	
