# Wizards buildenv-docker
Simple image for Docker-based builds, inspired by https://hub.docker.com/r/abxlabs/buildenv-docker/

## Contains  

* Docker
* [semver-tool](https://github.com/fsaintjacques/semver-tool.git)
* git

## Usage
We're using it as Docker agent in Jenkins pipelines for:

*  building images
*  determining semantic version and its segments
*  image tagging  
 
## Releases
Official image is available on Docker Hub:<br>
https://hub.docker.com/r/wizardsio/buildenv-docker

Tagging convention follows the official [Docker](https://hub.docker.com/_/docker) images releases. 
