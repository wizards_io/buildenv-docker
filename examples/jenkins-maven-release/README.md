# Wizards buildenv-docker
## Jenkins + Maven + Docker release example

Simple Jenkins pipeline that:

*  determines the Semantic Version, based on the current git tag
*  builds the Java application using official Maven image
*  builds the Docker image for the Java application
*  tags and publishes the image on the Docker Hub
 